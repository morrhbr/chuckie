function handleRejection(object, delegate) {
    var wrapper = {};
    var prototype = object;
    var propNames = [];
    do {
        propNames.push.apply(propNames, Object.getOwnPropertyNames(prototype));
    } while (prototype = Object.getPrototypeOf(prototype))
    var seen = {};
    propNames = propNames.filter(function (propName) {
            return Object.prototype.hasOwnProperty.call(seen, propName) ? false : (seen[propName] = true)
        }) // filter duplicate keys
        .forEach(function (key) {
            let f = object[key];
            if (typeof f === 'function') {
                Object.defineProperty(wrapper, key, {
                    value: async function () {
                        var args = Object.keys(arguments)
                            .filter(key => Object.prototype.hasOwnProperty.call(arguments, key))
                            .map(key => arguments[key]);
                        try {
                            var result = await f.apply(object, args);
                        } catch (err) {
                            delegate(err, key, args);
                        }
                        return result;
                    }
                });
            } else {
                Object.defineProperty(wrapper, key, {
                    value: f
                });
            }
        });
    return wrapper;
}
module.exports = handleRejection;
