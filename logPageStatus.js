const fs = require('fs');

async function directoryCreated(myPage, filePath, done){
    await myPage.screenshot({path: filePath+'.jpg', fullPage: true});
    done();
}

async function logPageStatus(myPage, filePath, done) {
    var htmlPath = filePath+'.html';
    myPage.content()
        .then(function (content) {
            fs.writeFile(htmlPath, content, function (err) {
                if (err) {
                    var directoryPath = htmlPath.split('/');
                    directoryPath.pop();
                    directoryPath = directoryPath.join('/');
                    fs.mkdir(directoryPath, function () {
                        fs.writeFile(htmlPath, content, function (err) {
                            if (err) {
                                console.log('error: ', err);
                            }
                            directoryCreated(myPage, filePath, done);
                        });
                    });
                } else {
                    directoryCreated(myPage, filePath, done);
                }
            });
        })
        .catch((err) => {
            console.log(err);
            done();
        });
}

module.exports = logPageStatus;
