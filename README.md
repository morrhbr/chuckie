# Chuckie

This is a framework with helper methods intended to ease building functional tests with puppeteer.
## handleRejection

Like people, APIs and package sometimes don't handle rejection very well. This can be a bother, especially when writing functional tests where cryptic unhandled Promise rejection can make for frustrating debugging. This class returns a copy of an input object with all it's functions wrapped in try catch block and will execute an input delegate in the catch block.

### Example

An usual test with puppeteer could look like this
```javascript
 it('Should be able to open the loginform', async function() {
        await myPage.goto(config.homeURL);
        await myPage.waitFor('[data-test-id="header-flyout__login-link"]');
        await myPage.click('[data-test-id="header-flyout__login-link"]');
        await myPage.waitFor('[data-test-id="loginform"]');
    });
```
If you wanted to display proper failure messages it would look something like this
```javascript
 it('Should be able to open the loginform', async function() {
        await myPage.goto(config.homeURL)
        .catch((err) => assert.fail('', '', 'Error waiting for homepage: ' + err));
        await myPage.waitFor('[data-test-id="header-flyout__login-link"]')
        .catch((err) => assert.fail('', '','Error waiting for selector [data-test-id="header-flyout__login-link"]: ' + err));
        await myPage.click('[data-test-id="header-flyout__login-link"]')
        .catch((err) => assert.fail('', '','Error trying to click on selector [data-test-id="header-flyout__login-link"]: ' + err));
        await myPage.waitFor('[data-test-id="loginform"]')
        .catch((err) => assert.fail('', '','Error waiting for selector [data-test-id="loginform"]: ' + err)));
    });
```
With handle-rejection it would look like this
```javascript
 it('Should be able to open the loginform', async function() {
        var delegate = function(error, methodName, args) {
            assert.fail('','', 'Function ' + methodName +
            ' rejected with arguments ' + JSON.stringify(args) +
            '\n' + error);
         };
        myPage = handleRejection(myPage);
        await myPage.goto(config.homeURL);
        await myPage.waitFor('[data-test-id="header-flyout__login-link"]');
        await myPage.click('[data-test-id="header-flyout__login-link"]');
        await myPage.waitFor('[data-test-id="loginform"]');
    });

```

## logPageStatus

Chuckie also provides a method that logs the HTML of a page to a file and takes a screenshot. This process runs async so you should pass it the done method of your mocha tests, so Chuckie can finish writing the file before mocha shuts down.
## Full example

```javascript
const puppeteer = require('puppeteer');
const faker = require('faker');
const config = require('./config');
const assert = require('chai').assert;
const handleRejection = require('./handleRejection');
const logPageStatus = require('./logPageStatus');

before(async function(done){
    faker.locale = "nl";
    browser = await puppeteer.launch(
       config(process.env)
    );
    var delegate = function(error, methodName, args) {
        assert.fail('','', 'Function ' + methodName +
    ' rejected with arguments ' + JSON.stringify(args) +
    '\n' + error);
    };
    myPage = await browser.newPage().then(page => handleRejection(page, delegate));
    done();
});


describe('Account login', function() {
    var firstFailed = false;
    it('Should be able to open the loginform', async function() {
        await myPage.goto(config.homeURL);
        await myPage.waitFor('[data-test-id="header-flyout__login-lin"]');
        await myPage.click('[data-test-id="header-flyout__login-link"]');
        await myPage.waitFor('[data-test-id="loginform"]');
    });

    afterEach(function (done) {
        if (!firstFailed && this.currentTest.state !== 'passed') {
            firstFailed = true;
            logPageStatus(myPage, 'test/functional/logs/account-fail', done);
        }else{
            done();
        }
    });
});

after(function() {
    if (!process.env.DEBUG) {
        browser.close();
    }
});
```